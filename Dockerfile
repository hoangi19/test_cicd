from python:3.10.6-slim-buster

# Install dependencies

COPY requirements.txt .

RUN pip install -r requirements.txt

# Copy source code
COPY main.py main.py

# Run the application
CMD ["python", "main.py"]
